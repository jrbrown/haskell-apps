{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Data.Randogram
    ( randogramG
    , randogram
    , randogramSeeded
    , randogramSt
    ) where


import System.Random (RandomGen, getStdGen, mkStdGen, split)
import Data.Bifunctor (first)
import Control.Monad.State as S (State, state)
import Data.Maybe (fromMaybe)
import Data.Distribution (sampleSeqUntilEmpty, bigramFromAscFile, distWithSequence)

import Content.Distributions (bigramAsciiCodes)


randogramG :: (RandomGen g) => g -> String -> IO String
randogramG g txt = do
    dM <- bigramFromAscFile "bigram.asc"
    let d = fromMaybe (error "bigram.asc had bad format, should be lines of space separated values of int int double where ints are ascii character codes (pred succ) and double is the weight.") dM
    let ds = distWithSequence d txt
    return . fst $ sampleSeqUntilEmpty ds g

randogram :: String -> IO String
randogram txt = do
    g <- getStdGen
    randogramG g txt

randogramSeeded :: Int -> String -> IO String
randogramSeeded n = randogramG (mkStdGen n)

randogramSt :: forall g. (RandomGen g) => State g (String -> IO String)
randogramSt = state func
    where
        func :: g -> (String -> IO String, g)
        func g = first randogramG $ split g

