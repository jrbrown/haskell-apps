module Main (main) where

import System.Random (getStdGen)
import Control.Monad.State (runState)
import Data.Randogram (randogramSt)

randogramForever :: IO ()
randogramForever = do
    g <- getStdGen
    let actions = sequence $ repeat randogramSt
    let (rndgrmFuncs, _) = runState actions g
    let ioActs = f <$> rndgrmFuncs
    sequence_ ioActs
        where f rndgrmFunc = do
                    txt <- getLine
                    out <- rndgrmFunc txt
                    putStrLn out

main :: IO ()
main = randogramForever
  

